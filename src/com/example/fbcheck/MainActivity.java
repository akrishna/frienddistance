package com.example.fbcheck;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.example.fbcheck.R;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;


public class MainActivity extends Activity {

	private Button loginButton;
	private CustomToast ct;
	private Dialog progressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ct=new CustomToast();
		setContentView(R.layout.activity_main);
		
		getActionBar().hide();
	//	Parse.initialize(this, "f6ZRw2JxRN0IFpoaK9wFsBdPrV50FWDQLqF7co6X", "IYDeerciVhU8vvEHG8v1jPJR70HCTE7WwlXpFVt0");
		Parse.initialize(this, "Fdytkav0EUAi1P1yX8PdqlxElpEjeH7cHXZIP9u5", "sd8RhpKxA4LRVc4zT5iOhi9ZSKuhKwCrrrAG5skc");
	//	PushService.setDefaultPushCallback(this, FriendActivity.class);
		ParseFacebookUtils.initialize(getString(R.string.app_id));
		//ParseAnalytics.trackAppOpened(getIntent());
		//PushService.setDefaultPushCallback(this, FriendActivity.class);
		//ParseInstallation.getCurrentInstallation().saveInBackground();

		// Check if there is a currently logged in user
				// and they are linked to a Facebook account.
				ParseUser currentUser = ParseUser.getCurrentUser();
				if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
					// Go to the user info acfshowUserDetailsActivity();
					showUserDetailsActivity();
				//	finish();
				}
				
		loginButton = (Button) findViewById(R.id.auth);
		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onLoginButtonClicked();
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
	
	private Request createRequest(Session session) {
	    Request request = Request.newGraphPathRequest(session, "me/friends", null);

	    Set<String> fields = new HashSet<String>();
	    String[] requiredFields = new String[] { "id", "name",
	            "installed" };
	    fields.addAll(Arrays.asList(requiredFields));
	    
	    Bundle parameters = request.getParameters();
	    parameters.putString("fields", TextUtils.join(",", fields));
	    request.setParameters(parameters);

	    return request;
	}
	
	
	private void onLoginButtonClicked() {
		progressDialog = ProgressDialog.show(
			MainActivity.this, "", "Logging in...", true);
		
		ct.showToast(MainActivity.this,"onclick");
		Log.d("hohoho",
				"onclick");
		List<String> permissions = Arrays.asList("basic_info", "user_about_me",
				"user_relationships", "user_birthday", "user_location","read_friendlists");
		Log.d("hohoho",
				"got permission");
		//showUserDetailsActivity();
		progressDialog.show();
		ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {
				progressDialog.dismiss();
				if (user == null) {
					Log.d("TAG",
							"Uh oh. The user cancelled the Facebook login.");
				} else if (user.isNew()) {
					Log.d("TAG",
							"User signed up and logged in through Facebook!");
					showUserDetailsActivity();
				} else {
					Log.d("TAG",
							"User logged in through Facebook!");
					showUserDetailsActivity();
				}
			}
		});
		//showUserDetailsActivity();
		ct.showToast(MainActivity.this,"finish");
	}

	private void showUserDetailsActivity() {
		
    /*	Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // handle response
                    	Log.d("accesstoken",Session.getActiveSession().getAccessToken());
                    	Log.d("msgfirst",response.toString());
                    	if(user!=null) {
                    		
                    		loggedInUser.put("fId",user.getId());
                    		userId=user.getId();
                    		loggedInUser.put("name",user.getName());
                    		handler.postDelayed(getLocationRunnable,50);
                    		
                    	}
                    	else {
                    		
                    	}
                    }
                });
    	Bundle bundle = request.getParameters();
	        bundle.putString("fields", "id,name");
	        request.setParameters(bundle);
        request.executeAsync();*/
		Intent intent = new Intent(this, FriendActivity.class);
		
		startActivity(intent);
		//finish();
	}
}
