package com.example.fbcheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphMultiResult;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.example.fbcheck.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainFragment extends Fragment{
	private View myView;
	private UiLifecycleHelper uiHelper;
	public View onCreateView(LayoutInflater inflater, 
	        ViewGroup container, 
	        Bundle savedInstanceState) {
		Log.d("onCreateView","yes");
		
	    View view = inflater.inflate(R.layout.activity_main, container, false);
	    myView=view;
	    uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	  //  LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
	   // authButton.setFragment(this);
	   // authButton.setReadPermissions(Arrays.asList("user_likes", "user_status"));
	    return view;
	}
	
   private Session.StatusCallback callback = new Session.StatusCallback() {
		    @Override
		    public void call(Session session, SessionState state, Exception exception) {
		    	Log.d("call","yes");
		    	//ct.showToast(MainActivity.this,"call ");
		        onSessionStateChange(session, state, exception);
		    }
    };
	@SuppressWarnings("deprecation")
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	    	//ct.showToast(MainActivity.this,"logged int");
	        Log.d("status", "Logged in..."+session.getAccessToken());
	        
	      
	          // make request to the /me API
	    /*    Request request = Request.newMyFriendsRequest(session, new Request.GraphUserListCallback() {

	            @Override
	            public void onCompleted(List<GraphUser> listFacebookFriends, Response response) {
	                
	            	Log.d("msg",listFacebookFriends.toString());
	            	//listFacebookFriends.get(0).getLocation().get
	            }
	        });

	        // here add fields explicitly 
	        Bundle bundle = request.getParameters();
	        bundle.putString("fields", "id,first_name,last_name,installed");
	        request.setParameters(bundle);
	        // execute like you did
	        request.executeAsync();*/
	    } else if (state.isClosed()) {
	    	//ct.showToast(MainActivity.this,"logged int");
	        Log.d("status", "Logged out..."+exception.getMessage()+"kk");
	    }
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	  Log.d("onCreate","yes");
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}
	
	private Request createRequest(Session session) {
	    Request request = Request.newGraphPathRequest(session, "me/friends", null);

	    Set<String> fields = new HashSet<String>();
	    String[] requiredFields = new String[] { "id", "name", "picture",
	            "installed" };
	    fields.addAll(Arrays.asList(requiredFields));

	    Bundle parameters = request.getParameters();
	    parameters.putString("fields", TextUtils.join(",", fields));
	    request.setParameters(parameters);

	    return request;
	}
	
	private List<GraphUser> getResults(Response response) {
	    GraphMultiResult multiResult = response
	            .getGraphObjectAs(GraphMultiResult.class);
	    GraphObjectList<GraphObject> data = multiResult.getData();
	    return data.castToListOf(GraphUser.class);
	}
	
	private List<GraphUser> filterFriends(List<GraphUser> friends)  {
		
		List<GraphUser> result=new ArrayList<GraphUser>();
		for(int i=0;i<friends.size();i++) {
			
			if(friends.get(0).getProperty("installed") != null) {
				result.add(friends.get(0));
			}
		}
		
		return result;
	}
}
