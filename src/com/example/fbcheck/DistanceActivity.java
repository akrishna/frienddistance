package com.example.fbcheck;

import java.util.ArrayList;
import java.util.List;

import com.example.fbcheck.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class DistanceActivity extends Activity {

	private ParseGeoPoint friendLocation;
	private ParseGeoPoint userLocation;
	private Handler handler;
	private String userId;
	private String friendId;
	private ProgressDialog pDialog;
	TextView distance;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_distance);
		handler=new Handler();
		userId=getIntent().getExtras().getString("userId");
		friendId=getIntent().getExtras().getString("friendId");
		distance=(TextView)findViewById(R.id.distance);
		handler.postDelayed(get_distance,50);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.distance, menu);
		return true;
	}
	
	public Runnable get_distance=new Runnable() {
    	
    	
    	public void run() {
			
    		pDialog=ProgressDialog.show(DistanceActivity.this,"","fetching Distance",true);
    		ParseQuery<ParseObject> query = ParseQuery.getQuery("appUser");
    		List<String> ids=new ArrayList<String>();
    		ids.add(userId);
    		ids.add(friendId);
    		Log.d("Ids",ids.toString());
    		
    		query.whereContainedIn("fId",ids);
    		query.findInBackground(new FindCallback<ParseObject>() {
    		    public void done(List<ParseObject> scoreList, ParseException e) {
    		        if (e == null) {
    		            Log.d("score", "Retrieved " + scoreList.size() + " scores");
    		            userLocation=(ParseGeoPoint) scoreList.get(0).get("location");
    		            friendLocation=(ParseGeoPoint) scoreList.get(1).get("location");
    		            Log.d("........",scoreList.get(1).getString("name")+scoreList.get(0).getString("name")+"");
    		            distance.setText("u are "+userLocation.distanceInKilometersTo(friendLocation)/1000 +" meters apart");
    		        } else {
    		            Log.d("score", "Error: " + e.getMessage());
    		        }
    		        pDialog.dismiss();
    		    }
    		});
    		
    		handler.postDelayed(get_distance,5000);
    			
		     }
			 
 
			 
    	
	};

}
