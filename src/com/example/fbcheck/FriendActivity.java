package com.example.fbcheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphMultiResult;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphUser;
import com.example.fbcheck.R;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FriendActivity extends Activity {

	ListView listView;
    CustomAdapter adapter;
    public  FriendActivity activity = null;
    private List<GraphUser> userList=new ArrayList<GraphUser>();
    private Session session;
    private CustomToast ct;
    private GPSTracker gps;
    private ParseObject userLocalObject;
    private String userCloudObjectId;
    private Handler handler;
    private TextView distanceTV;
    private ProgressDialog pDialog;
    private String userId;
    private DistanceCalculator mDistanceCalculator;
    ArrayList<ListModel> friendList=new ArrayList<ListModel>();
    private ParseGeoPoint point;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.friend_list);
        activity = this;
        handler=new Handler();
        gps=new GPSTracker(activity);
        userLocalObject=new ParseObject("AppUser");
        ct=new CustomToast();
        session=ParseFacebookUtils.getSession();
        userList=new ArrayList<GraphUser>();
        ParseUser currentUser = ParseUser.getCurrentUser();
        if((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
        	//ct.showToast(FriendActivity.this, "session is not null");
        	Request request = Request.newMeRequest(session,
                    new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            // handle response
                        	Log.d("accesstoken",Session.getActiveSession().getAccessToken());
                        	
                        	if(user!=null) {
                        		
                        		userLocalObject.put("fbId",user.getId());
                        		userId=user.getId();
                        		userLocalObject.put("fbName",user.getName());
                        		Log.d("msgfirst",response.toString()+user.getName());
                        		//check if this user already exist in parse
                        		ParseQuery<ParseObject> query = ParseQuery.getQuery("AppUser");
                		        query.whereEqualTo("fbId",userId);
                		        query.findInBackground(new FindCallback<ParseObject>() {
            		        	    public void done(List<ParseObject> object, ParseException e) {
            		        	    
            		        	    	if(object.size()==0) {
            		        	    		Log.d("imp","no object present"+userId);
            		        	    		updateFriendList();
            		        	    	}
            		        	    	else {
            		        	    		 userCloudObjectId=object.get(0).getObjectId();
            		        	    		 friendList=extractFriendsData(object.get(0).getJSONArray("friends"));
            		        	    		 loadFriendListView(friendList);
            		        	    	}
            		        	    	
            		        	    }
                		        });
                        		
                        		
                        	}
                        	else {
                        		
                        	}
                        }
                    });
        	Bundle bundle = request.getParameters();
 	        bundle.putString("fields", "id,name");
 	        request.setParameters(bundle);
            request.executeAsync();
        } 
          
	     // This is used for getting mutual friend in facebook
	       Bundle params = new Bundle();
	    
	        Request req = new Request(session, "me/mutualfriends/100001448778021", params, HttpMethod.GET, new Callback(){
	            @Override
	            public void onCompleted(Response response) {
	                //Log.d("just for chekc mutual friend ?",getResults(response).size()+" ");
	            }
	        });
	      
	        req.executeAsync();
	        
    }      
	    public void updateFriendList() { 
		      Request friendsRequest = createRequest(session);
			    friendsRequest.setCallback(new Request.Callback() {
		
			            @Override
			            public void onCompleted(Response response) {
			            	if(response!=null) {
			            		Log.d("msg",response.toString());
				                userList = getResults(response);
				              //  Log.d("msg",userList.size()+" "+userList.get(0).getName()+"&&"+userList.get(485).getName());
				                // TODO: your code here
				                JSONArray jsonArray=new JSONArray();
				         
				            	for(int i=0;i<userList.size();i++) {
				            		JSONObject jsonObject=new JSONObject();
				            		if(userList.get(i).getProperty("installed") != null) {
				            			try {
											jsonObject.put("userId",userList.get(i).getId());
											
											jsonObject.put("userName",userList.get(i).getName());
											//Log.d("here",jsonObject.getString("userId")+" "+jsonObject.getString("userName"));
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											//Log.d("error",e.getMessage());
										}
				            			jsonArray.put(jsonObject);
				            			friendList.add(new ListModel(userList.get(i).getId(),userList.get(i).getName()));
				            		}
				            		
				            	}
				            	Log.d("msg",jsonArray.toString());
				            	userLocalObject.put("friends",jsonArray);
				            	try {
				            		userLocalObject.put("value",100);
									userLocalObject.save();
									
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
				            	
				                loadFriendListView(friendList);
			            	} 
			            	else {
			            		
			            		ct.showToast(FriendActivity.this,"response null");
			            	}
			            }
			        });
			    friendsRequest.executeAsync();
		    
	       }
    
   public ArrayList<ListModel> extractFriendsData(JSONArray jsonArray) {
	   
	   ArrayList<ListModel> listModel=new ArrayList<ListModel>();
	   JSONObject jsonObject=new JSONObject();
	   for(int i=0;i<jsonArray.length();i++)  {
			try {
				jsonObject=jsonArray.getJSONObject(i);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			   try {
				listModel.add(new ListModel(jsonObject.getString("userId"),jsonObject.getString("userName")));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   return listModel;
	   
	   
   }
   /*****************  This function used by adapter ****************/
    public void onItemClick(int mPosition)
    {
    	
    /*	Intent intent=new Intent(FriendActivity.this,DistanceActivity.class);
    	intent.putExtra("friendId",list.get(mPosition).getId());
    	intent.putExtra("userId",userId);
    	startActivityForResult(intent,0);*/
    	final Dialog distancePopUp=new Dialog(FriendActivity.this);
    	distancePopUp.setTitle("Distances from"+friendList.get(mPosition).getName());
    	LayoutInflater factory = LayoutInflater.from(FriendActivity.this);
	    final View popupView = factory.inflate(R.layout.activity_distance, null);
    	distancePopUp.setContentView(popupView);
    	distanceTV=(TextView)popupView.findViewById(R.id.distance);
    	distancePopUp.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface arg0) {
				handler.removeCallbacks(mDistanceCalculator);
				
			}
    		
    	});
    	mDistanceCalculator=new DistanceCalculator(friendList.get(mPosition).getId());
        distancePopUp.show();
        handler.postDelayed(mDistanceCalculator,100);
       
        //ct.showToast(activity,list.get(mPosition).getId()+ "->"+list.get(mPosition).getName());
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friend, menu);
		return true;
	}
	
	public void loadFriendListView(ArrayList<ListModel> list) {
		
	
		 Resources res =getResources();
         listView= ( ListView )findViewById( R.id.list );  // List defined in XML ( See Below )

         adapter=new CustomAdapter( activity,list,res );
         listView.setAdapter( adapter );
         
         handler.postDelayed(getLocationRunnable,50);
	}
	private Request createRequest(Session session) {
	    Request request = Request.newGraphPathRequest(session, "me/friends", null);

	    Set<String> fields = new HashSet<String>();
	    String[] requiredFields = new String[] { "id", "name",
	            "installed" };
	    fields.addAll(Arrays.asList(requiredFields));
	    
	    Bundle parameters = request.getParameters();
	    parameters.putString("fields", TextUtils.join(",", fields));
	    request.setParameters(parameters);

	    return request;
	}
	
	private List<GraphUser> getResults(Response response) {
	    GraphMultiResult multiResult = response
	            .getGraphObjectAs(GraphMultiResult.class);
	    if(multiResult==null) {
	    	ct.showToast(FriendActivity.this,"result null");
	    	
	    }
	    GraphObjectList<GraphObject> data = multiResult.getData();
	    return data.castToListOf(GraphUser.class);
	}
	
	
	public Runnable getLocationRunnable=new Runnable() {
    	
    	
    	public void run() {
			 if(gps.canGetLocation()){
		         
		         double latitude = gps.getLatitude();
		         double longitude = gps.getLongitude();
		         point = new ParseGeoPoint(latitude,longitude);
		         userLocalObject.put("location",point);
		         ParseQuery<ParseObject> query = ParseQuery.getQuery("AppUser");
 		         query.whereEqualTo("fbId",userId);
 		         query.findInBackground(new FindCallback<ParseObject>() {
		        	    public void done(List<ParseObject> object, ParseException e) {
		        	    
		        	    	if(object.size()==0) {
		        	    		Log.d("check here","new object");
		        	    		 //userLocalObject.saveInBackground();
		        	    	}
		        	    	else {
		        	    		 Log.d("check here","old object"+point.getLatitude()+ "->"+point.getLongitude());
		        	    		 
		        	    		 object.get(0).put("location",point);
		        	    		 object.get(0).saveInBackground();
		        	    	}
		        	    	
		        	    }
 		        });
		    
		     }else{
		         // can't get location
		         // GPS or Network is not enabled
		         // Ask user to enable GPS/network in settings
		         gps.showSettingsAlert();
		     
			 
			 
		     }
			 handler.postDelayed(getLocationRunnable,5000);
    	}		 
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
	
	public class DistanceCalculator implements Runnable {
    	
		private String friendId;
		private ParseGeoPoint friendLocation;
		private ParseGeoPoint userLocation;
		
		public DistanceCalculator(String friendId) {
			this.friendId=friendId;
		}
		
    	public void run() {
			
    		pDialog=ProgressDialog.show(FriendActivity.this,"","fetching Distance",true);
    		ParseQuery<ParseObject> query = ParseQuery.getQuery("AppUser");
    		List<String> ids=new ArrayList<String>();
    		ids.add(userId);
    		ids.add(friendId);
    		Log.d("Ids",ids.toString());
    		
    		query.whereContainedIn("fbId",ids);
    		query.findInBackground(new FindCallback<ParseObject>() {
    		    public void done(List<ParseObject> scoreList, ParseException e) {
    		        if (e == null) {
    		        	try {
	    		            Log.d("score", "Retrieved " + scoreList.size() + " scores");
	    		            userLocation=(ParseGeoPoint) scoreList.get(0).get("location");
	    		            friendLocation=(ParseGeoPoint) scoreList.get(1).get("location");
	    		            Log.d("........",scoreList.get(1).getString("name")+scoreList.get(0).getString("name")+"");
	    		            distanceTV.setText("u are "+userLocation.distanceInKilometersTo(friendLocation)*1000+" meters apart");
    		        	}catch(IndexOutOfBoundsException ex) {
    		        		 Log.d("error",ex.getMessage());
    		        		 distanceTV.setText("there is some error with u or with your friend ,please choose another");
    		        	}
    		        	
    		        } else {
    		            Log.d("score", "Error: " + e.getMessage());
    		        }
    		        pDialog.dismiss();
    		    }
    		});
    		
    		handler.postDelayed(this,5000);
    			
      }
    	
  
 
			 
    	
	};
}
